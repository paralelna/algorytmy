#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

struct Element
{
    int value;
    struct Element* next;
    struct Element* prev;
};

struct L
{
    int how_many;
    struct Element* head;
};

void enqueue(int v, struct L** li){
    struct Element* help = malloc(sizeof(struct Element));
    help->value = v;
    if((*li)->head!=NULL){
        help->next = (*li)->head;
        help->prev = (*li)->head->prev;
        (*li)->head->prev->next = help;
        (*li)->head->prev = help;
        (*li)->how_many=(*li)->how_many+1;
    }
    else{
        help->next = help;
        help->prev = help;
        (*li)->head = help;
        (*li)->how_many=(*li)->how_many+1;
    }
}

int isempty(struct L** li){
    if((*li)->head==NULL){
        return 0;
    }
    else{
        return 1;
    }
}

void print(struct L** li){
    struct Element* help = (*li)->head;
    printf("Zawartosc kolejki : \n");
    int i = 0;
    while(i<(*li)->how_many){
        printf("%d \n", help->value);
        help=help->next;
        i++;
    }
    printf(" \n");
}

int get_value(struct L** li, int i){
    struct Element* help = (*li)->head;
    int how_many = (*li)->how_many;
    if(i>0 && i<=ceil((double)how_many/(double)2) && i<=how_many){
        int j = 1;
        while(j<i){
            help = help->next;
            j++;
        }
        return help->value;
    }
    else if(i>0 && i>ceil((double)how_many/(double)2) && i<=how_many){
        int j=how_many;
        while(j>=i){
            help = help->prev;
            j--;
        }
        return help->value;
    }
    else{
        return -1; //jak wyjdzie poza zakres
    }
}

void merge(struct L** li, struct L** li2){
    if((*li2)->head!=NULL && (*li)->head!=NULL){
        (*li)->how_many=(*li)->how_many+(*li2)->how_many;
        struct Element* help = (*li)->head->prev;
        struct Element* help2 = (*li2)->head;
        struct L* li3 = malloc(sizeof(struct L));
        li3->how_many=0;
        li3->head=NULL;
        int j=0;
        while(j < (*li2)->how_many){
            enqueue(help2->value,&li3);
            help2=help2->next;
            j=j+1;
        }
        if((*li)->head!=NULL){
            (*li)->head->prev->next=li3->head;
            li3->head->prev->next=(*li)->head;
            (*li)->head->prev=li3->head->prev;
            li3->head->prev=help;
        }
    }
}

void dequeue(struct L** li, int n){
    if((*li)->head!=NULL){
            struct Element* help = (*li)->head;
            if(n==1 && (*li)->how_many==1){
                (*li)->head = NULL;
                (*li)->how_many = 0;
            }
            else if(n==1 && (*li)->how_many>1){
                (*li)->head = help->next;
                (*li)->how_many = (*li)->how_many-1;
                help->prev->next=help->next;
                help->next->prev=help->prev;
            }
            else if(n>=2 && n<= (*li)->how_many){
                int how_many = (*li)->how_many;
                if(n<=how_many/2){
                    int j = 1;
                    while(j<n){
                        help = help->next;
                        j++;
                    }
                }
                else if(n>how_many/2){
                    int j=how_many;
                    while(j>=n){
                        help = help->prev;
                        j--;
                    }
                }
                help->prev->next=help->next;
                help->next->prev=help->prev;
                (*li)->how_many = (*li)->how_many - 1;
            }
    }
    else{
        printf("Kolejka pusta");
    }
}

int main()
{
    struct L* li = malloc(sizeof(struct L));
    li->how_many=0;
    li->head=NULL;

    /*struct L* li2 = malloc(sizeof(struct L));
    li2->how_many=0;
    li2->head=NULL;

    printf("Kolejka jest: %d\n", isempty(&li));
    enqueue(5, &li);
    printf("%d\n", get_value(&li, 1));
    enqueue(2, &li);
    enqueue(4, &li);
    enqueue(3, &li);
    enqueue(1, &li);
    enqueue(7, &li);
    printf("Kolejka jest dequeue: %d\n", isempty(&li));
    print(&li);
    dequeue(&li, 2);
    print(&li);
    dequeue(&li, 6);
    dequeue(&li, 3);
    dequeue(&li, 2);
    dequeue(&li, 1);
    dequeue(&li, 1);
    dequeue(&li, 1);
    print(&li);

    printf("HALLOOOO Kolejka jest: %d\n", isempty(&li));
    printf("%d\n", get_value(&li, 1));
    printf("%d\n", get_value(&li, 2));
    printf("%d\n", get_value(&li, 5));
    printf("Kolejka jest: tutaj 2 %d\n", isempty(&li2));
    enqueue(3, &li2);
    enqueue(6, &li2);
    enqueue(9, &li2);
    enqueue(10, &li2);
    enqueue(13, &li2);
    printf("Kolejka jest: tutaj 3 %d\n", isempty(&li2));
    merge(&li, &li);
    printf("Kolejka jest: tutaj koniec %d\n", isempty(&li));
    print(&li);
    print(&li2);*/

    srand (time(NULL));
    int n=10000;
    int i=0;
    while(i<n){
        enqueue(rand()%1000, &li);
        i++;
    }

    clock_t t;
    int k = 5000;
    i=0;
    t = clock();
    while(i<n){
        int a = get_value(&li,k);
        i++;
    }
    t = clock() - t;
    printf ("Dla indexu ustalonego k=%d w n= %d probach: %f sekund.\n",k,n,((float)t)/CLOCKS_PER_SEC/n);

    srand (1);
    clock_t t2;
    i=0;
    t2 = clock();
    while(i<n){
        k = rand()%n;
        //printf("%d\n",k);
        int z = get_value(&li,k);
        i++;
    }
    t2 = clock() - t2;
    printf ("Dla indexu losowanego w n= %d probach: %f sekund.\n",n,((float)t2)/CLOCKS_PER_SEC/n);

    return 0;
}
