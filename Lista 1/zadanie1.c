#include <stdio.h>
#include <stdlib.h>

struct Element
{
    int value;
    struct Element* next;
};

void enqueue(int v, struct Element** head, struct Element** tail){
    struct Element* help = malloc(sizeof(struct Element));
    help->value = v;
    help->next = NULL;
    if(*tail!=NULL){
        (*tail)->next = help;
        *tail = help;
    }
    else{
        *head = help;
        *tail = help;
    }
}

void dequeue(struct Element** head, struct Element** tail){
    if(*head!=NULL){
        struct Element* help = malloc(sizeof(struct Element));
        help = *head;
        *head = (*head)->next;
        if(*head==NULL) *tail = NULL;
        free(help);
    }
}

int isempty(struct Element** head){
    if(*head==NULL){
        return 0;
    }
    else{
        return 1;
    }
}

void print(struct Element** head){
    struct Element* help = malloc(sizeof(struct Element));
    help = *head;
    while(help!=NULL){
        printf("Element : %d\n", help->value);
        help=help->next;
    }
    free(help);
}

int main()
{
    struct Element* head = NULL;
    struct Element* tail = NULL;
    printf("Kolejka jest: %d\n", isempty(&head));
    enqueue(5, &head, &tail);
    enqueue(2, &head, &tail);
    printf("Kolejka jest: %d\n", isempty(&head));
    print(&head);
    dequeue(&head, &tail);
    print(&head);
    dequeue(&head, &tail);
    print(&head);
	dequeue(&head, &tail);
	print(&head);
    printf("Kolejka jest: %d\n", isempty(&head));
    return 0;
}