#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

struct Element
{
    int value;
    struct Element* next;
};

void enqueue(int v, struct Element** head){
    struct Element* help = malloc(sizeof(struct Element));
    help->value = v;
    help->next = NULL;
    if(*head!=NULL){
        struct Element* temp = *head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next = help;
    }
    else{
        *head = help;
    }
}

void dequeue(struct Element** head, int n){
    if(*head!=NULL){
            struct Element* help = *head;
            if(n==1){
                *head = help->next;
            }
            if(n>=2){
                int j = 1;
                while(help!=NULL){
                    if ((j+1)==n) break; //jesli na n-1 niz usuwana
                    help = help->next;
                    j++;
                }
                if (help->next->next==NULL) //czy nie jest ostatnia
                    help->next = NULL; // jesli jest
                else
                    help->next = help->next->next;
            }
    }
    else{
        printf("Kolejka pusta");
    }
}

int isempty(struct Element** head){
    if(*head==NULL){
        return 0;
    }
    else{
        return 1;
    }
}

int get_value(struct Element** head, int i){
    struct Element* help = *head;
    int j = 1;
    while(help!=NULL){
        if (j==i) break;
        help = help->next;
        j++;
    }
    if(j==i){
        return help->value;
    }
    else{
        return -1; //jak wyjdzie poza zakres
    }
}

void print(struct Element** head){
    struct Element* help = *head;
    printf("Zawartosc listy : \n");
    while(help!=NULL){
        printf(" %d \n", help->value);
        help=help->next;
    }
    printf(" \n");
}

void merge(struct Element** head, struct Element** head2){
    struct Element* help = *head;
    struct Element* help2 = *head2;
    struct Element* head3 = NULL;
    while(help2!=NULL){
        enqueue(help2->value,&head3);
        help2=help2->next;
    }
    while(help->next!=NULL){
        help=help->next;
    }
    if(head3!=NULL){
        help->next=head3;
    }
}

int main()
{
    struct Element* head = NULL;
    /*struct Element* head2 = NULL;
    printf("Kolejka jest: %d\n", isempty(&head));
    enqueue(5, &head);
    enqueue(2, &head2);
    enqueue(4, &head);
    enqueue(3, &head2);
    enqueue(1, &head);
    enqueue(7, &head2);
    printf("Kolejka jest: %d\n", isempty(&head2));
    print(&head);
    print(&head2);
    dequeue(&head, 2);
    print(&head);
    dequeue(&head, 1);
    print(&head);
    dequeue(&head, 1);
    print(&head);
    printf("Kolejka jest: %d\n", isempty(&head));
    printf("%d\n", get_value(&head2, 1));
    printf("%d\n", get_value(&head2, 2));
    printf("%d\n", get_value(&head2, 5));
    merge(&head, &head);
    print(&head);*/

    srand (time(NULL));
    int n=10000;
    int i=0;
    while(i<n){
        enqueue(rand()%1000, &head);
        i++;
    }

    clock_t t;
    int k = 5000;
    i=0;
    t = clock();
    while(i<n){
        int a = get_value(&head,k);
        i++;
    }
    t = clock() - t;
    printf ("Dla indexu ustalonego k=%d w n= %d probach: %f sekund.\n",k,n,((float)t)/CLOCKS_PER_SEC/n);

    srand (1);
    clock_t t2;
    i=0;
    t2 = clock();
    while(i<n){
        k = rand()%n;
        //printf("%d\n",k);
        int z = get_value(&head,k);
        i++;
    }
    t2 = clock() - t2;
    printf ("Dla indexu losowanego w n= %d probach: %f sekund.\n",n,((float)t2)/CLOCKS_PER_SEC/n);


    return 0;
}
